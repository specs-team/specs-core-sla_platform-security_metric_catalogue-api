/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.api.restfrontend;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.PersistenceException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import eu.specsproject.slaplatform.metricatalogue.utilities.PropertiesManager;
import eu.specsproject.slaplatform.metriccatalogue.MetricManagerFactory;
import eu.specsproject.slaplatform.metriccatalogue.api.restbackend.MetricAPI;
import eu.specsproject.slaplatform.metriccatalogue.entities.CollectionSchema;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetric;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;
import eu.specsproject.slaplatform.metriccatalogue.internal.PersistenceManager;

@Path("/security-metrics")
public class SMTsResource {

    @Context UriInfo uriInfo; 

    @GET
    @Produces({MediaType.APPLICATION_JSON })
    public String getSMTS(@QueryParam("db_name") String dbName, @QueryParam("metricType") String metricType, 
            @QueryParam("items") Integer totalItems, @QueryParam("page") Integer page, @QueryParam("length") Integer length) {
        MetricAPI metricApi = null;

        if(dbName != null){
            metricApi = MetricAPI.getInstance(dbName);
        }else{
            metricApi = MetricAPI.getInstance();
        }
        List <SecurityMetricIdentifier> smts = metricApi.getManager().searchSMTs(metricType);

        if(page == null && length == null && totalItems == null){
            totalItems = 50;
        }
        List <String> identifiers = new ArrayList<String>();
        int start = (page != null && length != null && totalItems == null) ? page : 0; 
        int stop = (totalItems != null) ? totalItems : smts.size(); 
        stop = (page != null && length != null && totalItems == null) ? page+length : stop; 
        stop = (stop > smts.size()) ? smts.size() : stop;
        identifiers = addIdentifiers(identifiers, start, stop, smts);

        CollectionSchema schema = new CollectionSchema("Security Metrics",smts.size(),stop-start,identifiers);

        return new Gson().toJson(schema);
    }

    private  List<String> addIdentifiers(List <String> identifiers, int start, int stop, List <SecurityMetricIdentifier> smts){
        for (int j = start; j < stop; j++){
            identifiers.add(uriInfo.getAbsolutePath()+"/"+smts.get(j).getId());
        }
        return identifiers;
    }

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.TEXT_PLAIN)
    public Response createSMT(String metricJson, @QueryParam("db_name") String dbName){
        MetricAPI metricApi = null;

        if(dbName != null){
            metricApi = MetricAPI.getInstance(dbName);
        }else{
            metricApi = MetricAPI.getInstance();
        }
        try {
            SecurityMetricIdentifier id = metricApi.getManager().createSMT(new SecurityMetricDocument(metricJson));
            UriBuilder ub = uriInfo.getAbsolutePathBuilder();
            URI slaURI = ub.
                    path(id.toString()).
                    build();
            return Response.created(slaURI).entity(id.toString()).build();
        } catch (Exception e) {
            e.printStackTrace();
            return Response.status(409).entity("Reference Id already exist").build();
        } 
        
    }

    @GET
    @Path("/backup/{dbName}.db")
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    public Response getSMTSBackup(@PathParam("dbName") String dbName) {

        try{    
            File downloadFile = MetricAPI.getInstance().getManager().getMetricsBackup(dbName);
            ResponseBuilder response = Response.ok((Object) downloadFile);
            return response.build();
        }catch(IllegalArgumentException e){
            return Response.serverError().entity(e.getMessage()).build();
        }


    }

    @POST
    @Path("/restore/{dbName}.db")
    @Consumes({MediaType.APPLICATION_OCTET_STREAM})
    public Response restoreSMTSBackup(InputStream dbFile, @PathParam("dbName") String dbName) {

        try{
            Boolean responseB = MetricAPI.getInstance().getManager().restoreMetricsBackup(dbFile, dbName);
            if(responseB){
                return Response.ok().build();
            }else{
                return Response.serverError().build();

            }
        }catch(IllegalArgumentException e){
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @Path("/{metric_id}.xml")
    @GET
    @Produces({MediaType.APPLICATION_OCTET_STREAM})
    public Response getSMTXml(@PathParam("metric_id") String metric_id, @QueryParam("db_name") String dbName){     
        try{
            MetricAPI metricApi = null;

            if(dbName != null){
                metricApi = MetricAPI.getInstance(dbName);
            }else{
                metricApi = MetricAPI.getInstance();
            }
            System.out.println("Retrieve metric");
            SecurityMetric metric = metricApi.getManager().retrieveSMT(new SecurityMetricIdentifier(metric_id));
            String xmlMetric = metric.getXMLdescription();
            System.out.println("Metric xml description returned: "+xmlMetric);

            try {
                String xmlFolder = PropertiesManager.getProperty("metric_catalogue.xmlPath");
                System.out.println("xmlFolder: "+xmlFolder);

                java.io.FileWriter fw = new java.io.FileWriter(xmlFolder+metric.getReferenceId()+".xml");
                fw.write(xmlMetric);
                fw.close();
                return Response.ok((Object) new File(xmlFolder+metric.getReferenceId()+".xml")).build();
            } catch (IOException e1) {
                LoggerFactory.getLogger(SMTsResource.class).debug("IOException", e1);
                return Response.serverError().entity(e1.getMessage()).build();
            }
        }catch(IllegalArgumentException | PersistenceException e){
            LoggerFactory.getLogger(SMTsResource.class).debug("IllegalArgumentException | PersistenceException", e);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @Path("/{id}")
    public SMTResource getSMTresource(@PathParam("id") String id) {
        return new SMTResource(id);
    }

}
