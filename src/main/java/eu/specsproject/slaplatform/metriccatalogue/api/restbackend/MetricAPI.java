/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.api.restbackend;

import eu.specsproject.slaplatform.metriccatalogue.MetricManager;
import eu.specsproject.slaplatform.metriccatalogue.MetricManagerFactory;
import eu.specsproject.slaplatform.metriccatalogue.api.restfrontend.utils.TestPersistence;


public class MetricAPI {

    private static MetricAPI instance = null;
    private MetricManager managerEU;
    private static String unitName = "";

    private MetricAPI(){
        managerEU = MetricManagerFactory.getMetricManagerInstance();
    }

    private MetricAPI(String dbName){
        managerEU = MetricManagerFactory.getMetricManagerInstance(dbName);
    }

    private MetricAPI(String unitName, boolean isTest){
        managerEU = MetricManagerFactory.getMetricManagerInstance(unitName, isTest);
    }

    public static MetricAPI getInstance() {
        if(!TestPersistence.isEnableTest()){
            if (instance==null || unitName != MetricManagerFactory.DEFAULTUNIT){
                unitName = MetricManagerFactory.DEFAULTUNIT;
                instance = new MetricAPI();
            }

            return instance;
        }else{
            if (instance==null || unitName != MetricManagerFactory.DEFAULTUNIT){
                unitName = MetricManagerFactory.DEFAULTUNIT;
                instance = new MetricAPI( "test_unit", true);
            }
            return instance;
        }
    }

    public static MetricAPI getInstance(String dbName) {
        if (instance==null || unitName != dbName){
            unitName = dbName;
            instance = new MetricAPI(dbName);
        }
        return instance;
    }

    public MetricManager getManager(){
        return managerEU;
    }


}
