/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.api.restfrontend;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import eu.specsproject.slaplatform.metriccatalogue.api.restbackend.MetricAPI;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;


public class SMTResource {

    private SecurityMetricIdentifier id ;

    public SMTResource (String id){
        this.id=new SecurityMetricIdentifier(id);
    }
 
    @GET
    @Produces({MediaType.APPLICATION_JSON })
    public String getSMT(@QueryParam("db_name") String dbName){
        MetricAPI metricApi = null;
        
        if(dbName != null){
            metricApi = MetricAPI.getInstance(dbName);
        }else{
            metricApi = MetricAPI.getInstance();
        }
        return new Gson().toJson(metricApi.getManager().retrieveSMT(id));
    }
    
    @PUT
    @Consumes(MediaType.TEXT_PLAIN)
    public Response updateSMT(String newSecurityMetric, @QueryParam("db_name") String dbName){
        MetricAPI metricApi = null;
        
        if(dbName != null){
            metricApi = MetricAPI.getInstance(dbName);
        }else{
            metricApi = MetricAPI.getInstance();
        }
        SecurityMetricDocument xml= null;
        
        if (newSecurityMetric!=null){
            xml = new SecurityMetricDocument(newSecurityMetric);
            SecurityMetricIdentifier identifier = metricApi.getManager().updateSMT(id, xml);
            return Response.ok(identifier.getId().toString()).build();
        }
        return Response.noContent().build();
    }
    
    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    public Response removeSMT(@QueryParam("db_name") String dbName){
        MetricAPI metricApi = null;
        
        if(dbName != null){
            metricApi = MetricAPI.getInstance(dbName);
        }else{
            metricApi = MetricAPI.getInstance();
        }
        metricApi.getManager().removeSMT(id);
        return Response.ok(id.toString()).build();
    }
    
    
    @Path("/annotations")
    public SMTAnnotationsResource getAnnotationResource(){
        return new SMTAnnotationsResource(id);
    }

}
