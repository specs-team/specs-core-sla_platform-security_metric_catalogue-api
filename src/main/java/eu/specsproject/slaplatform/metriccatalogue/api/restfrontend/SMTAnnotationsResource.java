/*
Copyright 2015 SPECS Project - CeRICT

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

@author  Massimiliano Rak massimilinao.rak@unina2.it
@author  Valentina Casola casolav@unina.it
@author  Pasquale De Rosa p.derosa@teamandroid.it
 */

package eu.specsproject.slaplatform.metriccatalogue.api.restfrontend;

import java.lang.annotation.Annotation;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import eu.specsproject.slaplatform.metriccatalogue.api.restbackend.MetricAPI;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;

public class SMTAnnotationsResource {
    
    private SecurityMetricIdentifier id ;

    public SMTAnnotationsResource (SecurityMetricIdentifier id){
        this.id=id;
    }

    @GET
    public Annotation getAnnotations(){
        
         MetricAPI.getInstance().getManager().getAnnotationsSMT(id);
        
         return null;
        
    }
    
    
    @POST
    public void createAnnotation(){
        MetricAPI.getInstance().getManager().annotateSMT(id, null);
    }
    
    
    @Path("/{annotationID}")
    @PUT
    public void updateAnnotation(@PathParam("annotationID") String annId){
        MetricAPI.getInstance().getManager().updateAnnotationSMT(id,annId,null);
    }
    
    
}
