package eu.specsproject.metriccatalogue.api.test;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.test.framework.AppDescriptor;
import com.sun.jersey.test.framework.JerseyTest;
import com.sun.jersey.test.framework.WebAppDescriptor;

import eu.specsproject.slaplatform.metriccatalogue.MetricManager;
import eu.specsproject.slaplatform.metriccatalogue.api.restbackend.MetricAPI;
import eu.specsproject.slaplatform.metriccatalogue.api.restfrontend.SMTAnnotationsResource;
import eu.specsproject.slaplatform.metriccatalogue.api.restfrontend.SMTResource;
import eu.specsproject.slaplatform.metriccatalogue.api.restfrontend.utils.CollectionResource;
import eu.specsproject.slaplatform.metriccatalogue.api.restfrontend.utils.SerializationProvider;
import eu.specsproject.slaplatform.metriccatalogue.api.restfrontend.utils.TestPersistence;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricDocument;
import eu.specsproject.slaplatform.metriccatalogue.entities.SecurityMetricIdentifier;
import eu.specsproject.slaplatform.metriccatalogue.internal.marshalling.MarshallingInterface;


public class MetricCatalogueApiTest extends JerseyTest{


    private static MetricAPI istance ;
    private static MetricManager manager ;
    private static SecurityMetricIdentifier id ;
    private static  SMTAnnotationsResource smt;
    private static SMTResource smtR;
    private static SerializationProvider sp ;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        TestPersistence.setEnableTest(true);
        JerseyEmbeddedHTTPServerCrunchify.startServer();
        System.out.println("setUpBeforeClass Called");
        int count = 0;
        istance = MetricAPI.getInstance();
        count++;
        Assert.assertNotNull(istance);
        System.out.println(count);
        manager = istance.getManager();
        count++;
        Assert.assertNotNull(manager);
        System.out.println(count);
        String  description = readFile("src/test/resources/AMD_timeliness.xml");
        id = manager.createSMT(new SecurityMetricDocument("{\"referenceId\" : \"AMD_timeliness.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}"));
        count++;
        Assert.assertNotNull(id);
        System.out.println(count);
        smt = new SMTAnnotationsResource(id);
        count++;
        Assert.assertNotNull(smt);
        System.out.println(count);
        smtR = new SMTResource(id.toString());
        count++;
        Assert.assertNotNull(smtR);
        System.out.println(count);
        sp = new SerializationProvider();
        count++;
        Assert.assertNotNull(sp);
        System.out.println(count);

    }


    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        //do nothing
    }


    @Before
    public void setUp() throws Exception {
       System.out.println("setUp Called");

    }

    @After
    public void tearDown() throws Exception {
        //System.out.println("tearDown Called");
    }
  
    @Test
    public void testgetIstance() {
    MetricAPI mAPI =    MetricAPI.getInstance();
    Assert.assertNotNull(mAPI); 
    
    }
    
    
    @Test
    public void testGetIstanceParameters () {
        MetricAPI mAPI;
        mAPI = MetricAPI.getInstance("");
        Assert.assertNotNull(mAPI);
    }

    
    @Test
    public void testgetManager(){
    Assert.assertNotNull(istance);
    MetricManager mm = istance.getManager();
    Assert.assertNotNull(mm);
    }

  
    @Test
    public void testSMTAnnotationsResource(){
         SMTAnnotationsResource smt1 = new  SMTAnnotationsResource(id);
        Assert.assertNotNull(smt1);
    }
    
    @Test
    public void testGetAnnotations(){
        smt.getAnnotations();
        Assert.assertTrue(true);        
    }
    
    @Test
    public void testCreateAnnotations(){
        smt.createAnnotation();
        Assert.assertTrue(true);        
    }
    
    @Test
    public void testUpdateAnnotations(){
        smt.updateAnnotation("");
        Assert.assertTrue(true);
    }
    
    @Test
    public void testSMTResource(){
        SMTResource smtRR = new SMTResource(id.toString());
        Assert.assertNotNull(smtRR);
    }
    
    @Test
    public void testGetSMT(){
        Assert.assertNotNull(smtR.getSMT(null));
        try {
            smtR.getSMT("");
        }
        catch ( Exception e) {
            Assert.assertTrue(true);
        }
    }
    
    @Test
    public void testUpdateSMT() throws IOException{
        String description = readFile("src/test/resources/AMD_Local.xml");
        String metricJSON = "{\"referenceId\" : \"AMD_Local.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}";
        Assert.assertEquals(smtR.updateSMT(metricJSON,null).getStatus(),200);
        try {
            smtR.updateSMT(null, null);
        }
        catch ( Exception e) {
            Assert.assertTrue(true);
        }
           try {
                smtR.updateSMT(null,"");
            }
            catch ( Exception e) {
                Assert.assertTrue(true);
            }
    }
    
    @Test
    public void testRemoveSMT(){
    SMTResource  smtRR = new SMTResource(id.toString());
        Assert.assertEquals(smtRR.removeSMT(null).getStatus(),200);
           try {
                smtR.removeSMT("");
            }
            catch ( Exception e) {
                Assert.assertTrue(true);
            }
        
    }
    
    @Test
    public void testGetAnnotationsResource(){
        Assert.assertNotNull(smtR.getAnnotationResource());
    }
    @Override
     protected AppDescriptor configure() {
        return new WebAppDescriptor.Builder().build();
    }
    
    @Test
    public void testMarshall() {
        List <String> lista = new ArrayList<String>();
        CollectionResource.marshall(lista);
        Assert.assertTrue(true);
        }
    
    @Test
    public void testUnMarshall(){
        CollectionResource.unMarshall("myTestCollection");
        Assert.assertTrue(true);
    }
       

    @Test
    public void testIsWriteable(){
        Assert.assertTrue(sp.isWriteable(MarshallingInterface.class,null,null,null));
    }
    
    @Test
    public void testIsReadable(){
        Assert.assertTrue(sp.isReadable(MarshallingInterface.class, null,null,null));
    }
/*
        @Test
    public void testGetPropValue() throws IOException{
        PropertiesManager pr = new PropertiesManager();
        Assert.assertNotNull(pr);
       String value = null;
        value = pr.getPropValue("");
       Assert.assertNotNull(value);
    }
    
    
    */
    @Test
    public void testReadFrom() throws WebApplicationException, IOException {  
        
            String xml = "myString";
            InputStream is = new ByteArrayInputStream(xml.getBytes());
            sp.readFrom(MarshallingInterface.class, null, null, MediaType.APPLICATION_XML_TYPE, null,is );  
            sp.readFrom(MarshallingInterface.class, null, null, MediaType.APPLICATION_JSON_TYPE, null,is ); 
        try{
            sp.readFrom(MarshallingInterface.class, null, null,null, null,is ); 
        }
        catch (Exception e ) {
            Assert.assertTrue(true);
        }


            Assert.assertTrue(true);
        
    } 
    
    
    @Test
    public void testGetSize(){
        Assert.assertNotNull(sp.getSize(null, null, null, null, null));
    }
    
    @Test
    public void testGetSMTS(){
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource web = client.resource("http://localhost:8089/security-metrics?metricType=abstract&items=10&page=0&length=2");
        ClientResponse response = web.get(ClientResponse.class);
        String responseGet = response.getEntity(String.class);
        Assert.assertEquals(200,response.getStatus());
        Assert.assertNotNull(responseGet); 
        web = client.resource("http://localhost:8089/security-metrics?metricType=abstract&items=10&page=0&length=2&dbname=mydb");
        response = web.get(ClientResponse.class);
        Assert.assertNotNull(response); 
      }
 
    @Test
    public void testGetSMTSBackup(){
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource web = client.resource("http://localhost:8089/security-metrics/backup/metric_catalogue.db");
        ClientResponse response = web.get(ClientResponse.class);
        String responseGet = response.getEntity(String.class);
        Assert.assertEquals(200,response.getStatus());
        Assert.assertNotNull(responseGet);       
        web = client.resource("http://localhost:8089/security-metrics/backup/metric_catalogue.db?dbname=mydb");
        response = web.get(ClientResponse.class);
        Assert.assertNotNull(response);
       
      }
     
    @Test
    public void testGetSMTXml() throws IOException{
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);
        WebResource  web = client.resource("http://localhost:8089/security-metrics/"+id.toString()+".xml");
        ClientResponse response = web.get(ClientResponse.class);
        String responseGet = response.getEntity(String.class);
        Assert.assertEquals(500,response.getStatus());
        Assert.assertNotNull(responseGet); 

        Assert.assertTrue(true);
    }
    
  @Test
    public void testGetSMTResource() throws IOException{
        System.out.println("id che inserisco:"+id.toString());
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);  
        WebResource web = client.resource("http://localhost:8089/security-metrics");
        String description = readFile("src/test/resources/AMD_Loc.xml");
        String metricJSON = "{\"referenceId\" : \"AMD_Loc.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}";
        ClientResponse     response = web.type(MediaType.TEXT_PLAIN).post(ClientResponse.class,metricJSON);
        Assert.assertNotNull(response);
        String responsePost = response.getEntity(String.class);
        Assert.assertNotNull(responsePost);
        web = client.resource("http://localhost:8089/security-metrics/"+responsePost);
        response = web.get(ClientResponse.class);
        Assert.assertNotNull(response);
        String responseGet = response.getEntity(String.class);
        Assert.assertEquals(500,response.getStatus());
        Assert.assertNotNull(responseGet); 
      }

  
    @Test
    public void testCreateSMT() throws IOException{     
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);      
        WebResource web = client.resource("http://localhost:8089/security-metrics");
        String description = readFile("src/test/resources/AMD_CertStatusRequest-2.xml");
        String metricJSON = "{\"referenceId\" : \"AMD_CertStatusRequest-2.xml\",\"XMLdescription\" : \""+description+"\" ,\"metricType\": \"abstract\"}";
        ClientResponse response = web.type(MediaType.TEXT_PLAIN).post(ClientResponse.class,metricJSON);
        String responsePost = response.getEntity(String.class);
        Assert.assertNotNull(responsePost);
        Assert.assertEquals(201,response.getStatus()); 
        SMTResource removing = new SMTResource("AMD_CertStatusRequest-2.xml");
        Assert.assertNotNull(removing);
        Assert.assertNotNull(removing.removeSMT(null));
        
    } 
    
    @Test
    public void testRestoreSMTsBackup(){        
        ClientConfig config = new DefaultClientConfig();
        Client client = Client.create(config);      
        WebResource web = client.resource("http://localhost:8089/security-metrics/restore/metric_catalogue.db");
        ClientResponse response = web.type(MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class,null);
        String responsePost = response.getEntity(String.class);
        Assert.assertNotNull(responsePost);
        Assert.assertEquals(500,response.getStatus()); 
        web = client.resource("http://localhost:8089/security-metrics/restore/anotherdb.db");
        response = web.type(MediaType.APPLICATION_OCTET_STREAM).post(ClientResponse.class,null);
        Assert.assertNotNull(response);
        
    } 
/*
    @Test
    public void testWriteTo (){
        OutputStream os = new ByteArrayOutputStream();
        try {
            sp.writeTo(new MarshallingInterface(){},MarshallingInterface.class, null, null, null, null, os);            
        } catch (WebApplicationException | IOException e) {         
            Assert.assertTrue(true);
        }
        try {
            sp.writeTo(new MarshallingInterface () {}, MarshallingInterface.class, null, null , MediaType.APPLICATION_XML_TYPE, null, os);
        } catch (WebApplicationException | IOException e) {         
            Assert.assertTrue(true);
        }
        try {
            sp.writeTo(new MarshallingInterface () {}, MarshallingInterface.class, null, null , MediaType.APPLICATION_JSON_TYPE, null, os);
        } catch (WebApplicationException | IOException e) {
            Assert.assertTrue(true);
        }
    }
  */
    

    private static String readFile(String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            return sb.toString().replace("\"","\\\"");
        } finally {
            br.close();
        }
    }

}
