<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Metric Catalogue REST Client</title>
<script type="text/javascript">
	var api_path = "metric_catalogue_rest_api/";

	function call(method) {
		var xmlhttp;
		var url = "/metric_manager_jaxrs/"+api_path + document.getElementById('apis').value;

		if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp = new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}

		/* 		xmlhttp.onreadystatechange = function() {
		 if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		 document.getElementById("result").innerHTML = xmlhttp.responseText;
		 }
		 } */
/***************************************************************************/
		
		 xmlhttp.open(method, url, false);

/********headers********/
		var radios = document.getElementsByName('content_type');
		var radio_val = "NO";
		// loop through list of radio buttons
		for (var i = 0, len = radios.length; i < len; i++) {
			if (radios[i].checked) { // radio checked?
				radio_val = radios[i].value; // if so, hold its value in val
				break; // and break out of for loop
			}
		}

		if (radio_val != "NO")
			if (radio_val == "custom")
				xmlhttp.setRequestHeader("Content-Type", document
						.getElementById("content_type_custom").value);
			else
				xmlhttp.setRequestHeader("Content-Type", radio_val);

		if (document.getElementById("accept_header").checked)
			xmlhttp.setRequestHeader("Accept", document
					.getElementById("accept_custom").value);
		
		var table = document.getElementById("headers_table");
		 
		if (table.rows.length>1){
			
			var row;
			for (var i=1; i < table.rows.length; i++ ) {
				row = table.rows[i];
				if(row.cells[2].getElementsByTagName("input")[0].checked){
					var header_name = row.cells[0].getElementsByTagName("input")[0].value;
					if (header_name!= '' && header_name!=null)
						xmlhttp.setRequestHeader(header_name,row.cells[1].getElementsByTagName("input")[0].value);					
				}
					
			}
				
			
		}
/******************************************************************/


xmlhttp.send(document.getElementById("postbody").value);

		document.getElementById("head").innerHTML = "";
		document.getElementById("result").innerHTML = "";
		document.getElementById("xml_res").innerHTML = "";
		document.getElementById("status").innerHTML = "";
		document.getElementById("statustxt").innerHTML = "";

		document.getElementById("head").innerHTML = xmlhttp
				.getAllResponseHeaders();
		document.getElementById("result").innerHTML = xmlhttp.responseText;
		if (xmlhttp.responseXML)
			document.getElementById("xml_res").innerHTML = (new XMLSerializer())
					.serializeToString(xmlhttp.responseXML);
		document.getElementById("status").innerHTML = xmlhttp.status;
		document.getElementById("statustxt").innerHTML = xmlhttp.statusText;
	}

	function addrow(table_id) {
		var table = document.getElementById(table_id);
		var row = table.insertRow(-1);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);
		var cell3 = row.insertCell(2);
		var cell4 = row.insertCell(3);

		cell3.innerHTML = "<input type='checkbox'/>";
		cell1.innerHTML = "<input type='text'/>";
		cell2.innerHTML = "<input type='text'/>";
		cell4.innerHTML = "<button onclick='delRow();'>remove</button>";
	}

	function delRow() {
		var current = window.event.srcElement;
		//here we will delete the line
		while ((current = current.parentElement) && current.tagName != "TR")
			;
		current.parentElement.removeChild(current);
	}
</script>



</head>
<body>
	<script language="javascript">
		document.write(api_path);
	</script>
	<!-- <select id="apis">
</select>
<script language="javascript">
/*var select = document.getElementById("apis"); 
for(var i = 0; i < apis.length; i++) {
 
    var el = document.createElement("option");
    el.textContent = apis[i];
    el.value = apis[i];
    select.appendChild(el);*/
}
</script> -->
	<input type="text" id="apis" />


	<table>
		<tr>
			<td><button onclick="call('GET')">GET</button></td>
			<td><button onclick="call('POST')">POST</button></td>
		</tr>
		<tr>
			<td><button onclick="call('PUT')">PUT</button></td>
			<td><button onclick="call('DELETE')">DELETE</button></td>
		</tr>
		<tr>
			<td><button onclick="call('HEAD')">HEAD</button></td>
			<td><button onclick="call('OPTIONS')">OPTIONS</button></td>
		</tr>
		<tr>
			<td><button onclick="call('PATCH')">PATCH</button></td>
		</tr>
	</table>

<br/>

	<table>
		<tr>
			<td>Headers:</td>
		</tr>

		<tr>
			<td></td>
			<td><input type="checkbox" id="accept_header" />Accept: <input
				type="text" id="accept_custom" /></td>
		</tr>
		<tr>
			<td></td>
			<td><input id="app_json" type="radio" name="content_type"
				value="NO" checked="checked" />Content-Type: NO</td>
		</tr>
		<tr>
			<td></td>
			<td><input id="app_json" type="radio" name="content_type"
				value="application/json" />Content-Type: application/json</td>
		</tr>
		<tr>
			<td></td>
			<td><input id="app_json" type="radio" name="content_type"
				value="application/xml" />Content-Type: application/xml</td>
		</tr>
		<tr>
			<td></td>
			<td><input id="app_json" type="radio" name="content_type"
				value="custom" />Content-Type: <input type="text"
				id="content_type_custom" /></td>
		</tr>


		<tr>
			<td></td>
			<td>
				<table border="1" id="headers_table">
					<tr>
						<td>Header Name</td>
						<td>Header Value</td>
						<td>activate</td>
						<td>
							<button onclick="addrow('headers_table')">add</button>
						</td>
					</tr>
				</table>
			</td>
		</tr>

	</table>
	<br />
	<br /> Request Message Body:<br />
	<textarea id="postbody" rows="6" cols="100"></textarea>
	<br />
	<br />





	<h2>HEADERS</h2>
	<div id="status"></div>
	<div id="statustxt"></div>
	<div id="head"></div>

	<h2>RESULT</h2>
	<xmp id="result"></xmp>
	<xmp id="xml_res"></xmp>


</body>
</html>